-- Support for default torches
minetest.override_item("default:torch", {
	drawtype = "torchlike",
	mesh = "",
})
minetest.override_item("default:torch_wall", {
	drawtype = "torchlike",
	mesh = "",
	tiles = {{
		name = "default_torch_animated.png",
		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
	}},
})
minetest.override_item("default:torch_ceiling", {
	drawtype = "torchlike",
	mesh = "",
	tiles = {{
		name = "default_torch_on_ceiling_animated.png",
		animation = {type = "vertical_frames", aspect_w = 16, aspect_h = 16, length = 3.3}
	}},
})

-- Support for Real Torches mod
--
-- NOTE: it's true that real_torch mod has 2D version,
-- but it's used ONLY on old Minetest Game versions,
-- before 3D torches were added.
--
if minetest.get_modpath("real_torch") then
	minetest.override_item("real_torch:torch", {
		drawtype = "torchlike",
		mesh = "",
	})
	minetest.override_item("real_torch:torch_wall", {
		drawtype = "torchlike",
		mesh = "",
		tiles = {"real_torch_wall.png"},
	})
	minetest.override_item("real_torch:torch_ceiling", {
		drawtype = "torchlike",
		mesh = "",
		tiles = {"real_torch_ceiling.png"},
	})
end
