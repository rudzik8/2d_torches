# 2D Torches [2d_torches]
Simple mod for MTG that makes default torches look 2D (just like in older versions)

**Depends:** default

**Optional depends:** real_torch

![Screenshot](screenshot.png)

## License
Code is licensed under Unlicense. See LICENSE file or <https://unlicense.org/> for details. Thanks.
